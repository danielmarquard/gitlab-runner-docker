FROM gitlab/gitlab-runner:alpine

RUN \
  apk update -Uv --no-progress && \
  apk add -uv --no-progress \
    ca-certificates && \
  rm -rf /var/cache/apk/*

COPY certs/ca-proxy-cert.pem /usr/local/share/ca-certificates/ca-proxy-cert.crt

RUN update-ca-certificates